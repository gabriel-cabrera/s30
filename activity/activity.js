// For # 1

db.fruits.aggregate([
    {
        $match: {onSale: true}
    },
    {$count: "fruitsOnSale"}
    ]);


// For # 2

db.fruits.aggregate([
    {
        $match: {stock: supplier_id}
    },
    {$count: "enoughStock"}
    ]);


// For # 3

db.fruits.aggregate([
        {
            $match: { onSale: true }
        },
        {
            $group: {
                _id: "$supplier_id",
                avg_price: {$avg: "$price"}
            }
        }
    ]);


// For # 4

db.fruits.aggregate([
        {
            $match:  {onSale: true}
        },
        {
            $group: {
                _id: "$supplier_id",
                max_price: {$max: "$price"}
            }
        }
    ]);

// For # 5

db.fruits.aggregate([
        {
            $match:  {onSale: true}
        },
        {
            $group: {
                _id: "$supplier_id",
                min_price: {$min: "$price"}
            }
        }
    ]);